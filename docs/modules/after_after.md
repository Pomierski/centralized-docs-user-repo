[Documentation](../README.md) / [Exports](../modules.md) / after/after

# Module: after/after

## Table of contents

### Functions

- [default](after_after.md#default)

## Function

### default

▸ **default**(`n`, `func`): (`this`: `any`, ...`args`: `any`[]) => `any`

The opposite of `before`. This method creates a function that invokes
`func` once it's called `n` or more times.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `any` | The number of calls before `func` is invoked. |
| `func` | `Function` | The function to restrict. |

#### Returns

`fn`

Returns the new restricted function.

▸ (`this`, `...args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `this` | `any` |
| `...args` | `any`[] |

##### Returns

`any`

**`Since`**

0.1.0

**`Example`**

```ts
const saves = ['profile', 'settings']
const done = after(saves.length, () => console.log('done saving!'))

forEach(saves, type => asyncSave({ 'type': type, 'complete': done }))
// => Logs 'done saving!' after the two async saves have completed.
```

#### Defined in

[after/after.ts:18](https://gitlab.com/Pomierski/centralized-docs-user-repo/-/blob/892bc56/src/utils/after/after.ts#L18)
