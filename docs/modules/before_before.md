[Documentation](../README.md) / [Exports](../modules.md) / before/before

# Module: before/before

## Table of contents

### Functions

- [default](before_before.md#default)

## Function

### default

▸ **default**(`n`, `func`): (...`args`: `any`[]) => `any`

Creates a function that invokes `func`, with the `this` binding and arguments
of the created function, while it's called less than `n` times. Subsequent
calls to the created function return the result of the last `func` invocation.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `n` | `any` | The number of calls at which `func` is no longer invoked. |
| `func` | `any` | The function to restrict. |

#### Returns

`fn`

Returns the new restricted function.

▸ (`...args`): `any`

##### Parameters

| Name | Type |
| :------ | :------ |
| `...args` | `any`[] |

##### Returns

`any`

**`Since`**

3.0.0

**`Example`**

```ts
jQuery(element).on('click', before(5, addContactToList))
// => Allows adding up to 4 contacts to the list.
```

#### Defined in

[before/before.ts:16](https://gitlab.com/Pomierski/centralized-docs-user-repo/-/blob/892bc56/src/utils/before/before.ts#L16)
