[Documentation](../README.md) / [Exports](../modules.md) / clamp/clamp

# Module: clamp/clamp

## Table of contents

### Functions

- [default](clamp_clamp.md#default)

## Number

### default

▸ **default**(`number`, `lower`, `upper`): `number`

Clamps `number` within the inclusive `lower` and `upper` bounds.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `number` | `number` | The number to clamp. |
| `lower` | `number` | The lower bound. |
| `upper` | `number` | The upper bound. |

#### Returns

`number`

Returns the clamped number.

**`Since`**

4.0.0

**`Example`**

```ts
clamp(-10, -5, 5)
// => -5

clamp(10, -5, 5)
// => 5
```

#### Defined in

[clamp/clamp.ts:18](https://gitlab.com/Pomierski/centralized-docs-user-repo/-/blob/892bc56/src/utils/clamp/clamp.ts#L18)
