[Documentation](../README.md) / [Exports](../modules.md) / create/create

# Module: create/create

## Table of contents

### Functions

- [default](create_create.md#default)

## Object

### default

▸ **default**(`prototype`, `properties?`): `any`

Creates an object that inherits from the `prototype` object. If a
`properties` object is given, its own enumerable string keyed properties
are assigned to the created object.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `prototype` | `any` | The object to inherit from. |
| `properties?` | `any` | The properties to assign to the object. |

#### Returns

`any`

Returns the new object.

**`Since`**

2.3.0

**`Example`**

```ts
function Shape() {
  this.x = 0
  this.y = 0
}

function Circle() {
  Shape.call(this)
}

Circle.prototype = create(Shape.prototype, {
  'constructor': Circle
})

const circle = new Circle
circle instanceof Circle
// => true

circle instanceof Shape
// => true
```

#### Defined in

[create/create.ts:33](https://gitlab.com/Pomierski/centralized-docs-user-repo/-/blob/892bc56/src/utils/create/create.ts#L33)
