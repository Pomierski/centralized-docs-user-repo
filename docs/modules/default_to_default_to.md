[Documentation](../README.md) / [Exports](../modules.md) / default-to/default-to

# Module: default-to/default-to

## Table of contents

### Functions

- [default](default_to_default_to.md#default)

## Util

### default

▸ **default**(`value`, `defaultValue`): `any`

Checks `value` to determine whether a default value should be returned in
its place. The `defaultValue` is returned if `value` is `NaN`, `null`,
or `undefined`.

#### Parameters

| Name | Type | Description |
| :------ | :------ | :------ |
| `value` | `any` | The value to check. |
| `defaultValue` | `any` | The default value. |

#### Returns

`any`

Returns the resolved value.

**`Since`**

4.14.0

**`Example`**

```ts
defaultTo(1, 10)
// => 1

defaultTo(undefined, 10)
// => 10
```

#### Defined in

[default-to/default-to.ts:19](https://gitlab.com/Pomierski/centralized-docs-user-repo/-/blob/892bc56/src/utils/default-to/default-to.ts#L19)
